/*point2d.hpp*/
/*Plik nag³ówkowy klasy Point2d.hpp*/
#ifndef POINT2D_HPP_INCLUDED
#define POINT2D_HPP_INCLUDED
#include <iostream>

class Point2d{
    public:
        //konstruktor bez argumentow zwraca punkt (0,0)
        Point2d();

        //destruktor nie jest zadeklarowany
        //bedzie wykorzystywany domyslny
        //~Point2d();

        //konstruktor pobierajacy wspolrzedne
        Point2d(double, double);

        //konstruktor kopiujacy
        Point2d(const Point2d& other);

        //przeciazony operator przypisania
        Point2d& operator= (const Point2d& other);

        //gettery i settery
        double getX();
        double getY();
        double getR();
        double getPhi();
        void setXY(double, double);
        void setRPhi(double, double);
        void jednok (double);
        void obrot (double);

    private:
        //wspolrzedne punktu jako pola prywatne
        double _x;
        double _y;
        double _r;
        double _phi;
};

//deklaracja przeciazonego operatora <<
//na potrzeby wyœwietlania wspolrzednych punktu
std::ostream& operator<<(std::ostream& out, Point2d& p);

#endif // POINT2D_HPP_INCLUDED
