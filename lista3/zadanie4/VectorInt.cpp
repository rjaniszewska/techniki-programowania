#include "VectorInt.h"
#include <iostream>
#include <string>
#include <sstream>

VectorInt::VectorInt()
{
    _dlugosc=16;
    _size=0;
    _capacity=_dlugosc;  //cala pamiec jest wolna
    _wek = new int [_dlugosc]; //alokacja pamieci
    for(int i=0; i<_dlugosc; i++)
    {
        _wek[i]=0;  //ustawia wszystkim mozliwym elementom 0 bo jest to zerowy pusty wektor
    }

}
VectorInt::VectorInt(int n)
{
    _dlugosc=n;
    _size=0;
    _capacity=_dlugosc;
    _wek = new int [_dlugosc];

    for(int i=0;i<_dlugosc;i++)
        _wek[i]=0;
}

VectorInt::VectorInt(const VectorInt& inny) //konstruktor kopiujacy
{
    this->_dlugosc=inny._dlugosc;
    this->_size=inny._size;
    this->_capacity=inny._capacity;
    this->_wek=new int [_dlugosc];

    for(int i=0; i<this->_dlugosc; i++)
    {
        this->_wek[i]=inny._wek[i]; // kopiowanie wartosci z tablicy jednego obiektu do drugiego
    }
}

VectorInt& VectorInt::operator= (const VectorInt& inny ) //operator przypisania
{
    if(this!=&inny)
    {
        delete this->_wek;
        this->_dlugosc=inny._dlugosc;
        this->_size=inny._size;
        this->_capacity=inny._capacity;
        this->_wek=new int[_dlugosc];

        for(int i=0; i<this->_dlugosc; i++)
            this->_wek[i]=inny._wek[i];

    }
    return *this;
}

VectorInt::~VectorInt()  //destruktor bozy, ktory czysci pamiec
{
    _dlugosc=0;
    _size=0;
    _capacity=0;
    delete [] _wek; //czyscimy tablice
}

int VectorInt::at(int n) //wypisuje wartosc na n-tym miejscu
{
    return _wek[n-1]; //uzywamy normalnych liczb, tzn 1 dla pierwszego miejsca, a nie 0
}

void VectorInt::insert(int miejsce, int wartosc) //wpisuje wartosc na dane miejsce
{
    if (miejsce<=0)
        std::cout<<std::endl<<"Indeksujemy od 1, tzn np dla tab[0] wpisujemy 1, a dla tab[1] 2";

    else if(miejsce>_dlugosc)
    {
        realok(miejsce);
        _wek[miejsce-1]=wartosc;
    }

    else //jek sie miesci w pamieci
        _wek[miejsce-1]=wartosc;

    reg_size();
}

void VectorInt::pushback(int n)  //dodawanie kolejnego elementu
{
    if(_capacity==0)
    {
        realok(_dlugosc+1);
        _wek[_dlugosc-1]=n;
    }
    else  //jesli jest miejsce
    {
        _wek[_size]=n;
    }
    reg_size();
}
void VectorInt::popback() //usuwanie ostatniego elementu wektora
{
    if(_size>0)
    {
        _wek[_size-1]=0;
        reg_size();
    }
    else
    {
        std::cout<<std::endl<<"Nie mozna skrocic zerowego wektora :(";
    }
}

void VectorInt::shrinkToFit()
{
    reg_size();
    realok(_size);
    _capacity=0;
}

void VectorInt::clear() //czyszczenie wszystkich elemtow i sprawdzenie, czy wketor jest pusty
{
    for(int i=0;i<_dlugosc;i++)
    {
        _wek[i]=0;
    }
    reg_size();
}
int VectorInt::size()
{
    return _size;
}

int VectorInt::capacity()
{
    return _capacity;
}

int VectorInt::get_dlugosc()
{
    return _dlugosc;
}

void VectorInt::realok(int n)
{
    int tab[n];

    for(int i=0;i<n;i++)
    {
        if(i<_dlugosc)
            tab[i]=_wek[i];
        else
            tab[i]=0;
    }
    delete [] _wek;
    _dlugosc=n;
    _wek = new int [_dlugosc];

    for(int i=0; i<_dlugosc; i++)
        _wek[i]=tab[i];
}

void VectorInt::reg_size()
{
    bool pom=true;

    for(int i=_dlugosc-1; i>=0; i--)
    {
        if(_wek[i]!=0)
        {
            _size=i+1;
            pom=false;
            break;
        }
    }
    if(pom) _size=0;
    _capacity=_dlugosc-_size;
}

//-----------------------------------------------------------------------------------

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

std::ostream& operator<<(std::ostream& out, VectorInt& wek)
{
    std::string ds="[ ";
    for(int i =1; i<wek.size(); i++)
    {
        ds=ds+ patch::to_string(wek.at(i))+", ";
    }
    ds=ds+ patch::to_string(wek.at(wek.size()))+" ]";
    return out<<ds;
}
