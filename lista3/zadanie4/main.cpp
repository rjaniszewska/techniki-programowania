#include <iostream>
#include "VectorInt.h"
#include <conio.h>
#include <windows.h>
using namespace std;

int main()
{
    bool dziala=true;
    bool exist=false;
    VectorInt *wektor;
    char opcje;

    do
    {
        if(!exist)
        {
            cout<<"Program do generowania wektorow";
            cout<<endl<<endl;
            cout<<"Wektor jeszcze nie powstal"<<endl<<"Na ile wymiarow zaalokowac pamiec: ";
            int w;
            while(!(cin>>w))
            {
                cout<<"Blad - prosze podac liczbe calkowita: ";
                cin.clear();
                cin.ignore(9999, '\n' );
            }
            wektor=new VectorInt(w);
            system("cls");
            exist=true;
        }
        cout<<"Program do generowania wektorow";
        cout<<endl<<endl;

        if(wektor->size()==0)
        {
            cout<<"Wektor jeszcze nie powstal";
        }

        else
        {
            cout<<"Twoj wektor: "<<(*wektor);
        }

        cout<<endl<<"Zaalokowano pamiec na miejsc: "<<wektor->get_dlugosc();
        cout<<endl<<"Liczba wymiarow: "<<wektor->size();
        cout<<endl<<"Liczba wolnych miejsc: "<<wektor->capacity();

        cout<<endl;
        cout<<endl<<"1. Wyswietl dany element wektora.";
        cout<<endl<<"2. Dopisz wartosc.";
        cout<<endl<<"3. Dodaj kolejny wymiar wektora.";
        cout<<endl<<"4. Usun ostatni wymiar wektora.";
        cout<<endl<<"5. Wyczysc wszystkie wymiary.";
        cout<<endl<<"6. Usun niewykorzystywana pamiec.";
        cout<<endl<<"7. Wyjdz z programu.";
        cout<<endl<<endl<<"Wybieram: ";
        cin>>opcje;

        switch(opcje)
        {
        case '1':
            {
                bool w=false;
                int licz;

                while(!w)
                {
                    cout<<endl<<"Ktory element chcesz zobaczyc? (1-"<<wektor->get_dlugosc()<<")";
                    cout<<endl<<"Wpisz numer pola: ";
                    while(!(cin>>licz))
                    {
                        cout<<"Blad - prosze podac liczbe calkowita: ";
                        cin.clear();
                        cin.ignore(9999, '\n' );
                    }
                    if((licz<=0) || (licz>wektor->get_dlugosc()))
                        cout<<endl<<"Podaj liczbe z zakresu!";
                    else
                        w=true;

                }
                cout<<"v_"<<licz<<" = "<<wektor->at(licz);
                getch();
                break;
            }

        case '2':
            {
                int licz1, licz2;

                cout<<endl<<"Na ktorym miejscu chcesz wstawic wartosc: ";
                while(!(cin>>licz1))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                cout<<endl<<"Jaka wartosc chcesz tam wstawic: ";

                while(!(cin>>licz2))
                {
                    cout<<"Blad - prosze podac liczbe ca³kowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                wektor->insert(licz1,licz2);
                break;
            }

        case '3':
            {
                int licz;

                cout<<endl<<"Jaka wartosc chcesz wstawic: ";
                while(!(cin>>licz))
                {
                    cout<<"Blad - prosze podac liczbe calkowita: ";
                    cin.clear();
                    cin.ignore(9999, '\n' );
                }
                wektor->pushback(licz);
                break;
            }

        case '4':
            {
                wektor->popback();
                break;
            }

        case '5':
            {
                wektor->clear();
                break;
            }

        case '6':
            {
                wektor->shrinkToFit();
                break;
            }

        case '7':
            {
                delete wektor;
                dziala=false;
                break;
            }

        default:
            {
                cout<<endl<<"Wybierz prawidlowa opcje!";
                getch();
                break;
            }
        }
        system("cls");
    }while(dziala);

    return 0;
}
