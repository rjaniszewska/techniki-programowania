#ifndef VECTORINT_H_INCLUDED
#define VECTORINT_H_INCLUDED
#include <iostream>

 class VectorInt
 {
 public:
    VectorInt(); //konstruktor pusty
    VectorInt(int);  //konstruktor z wartoscia
    VectorInt(const VectorInt&);  //kopiujacy
    VectorInt& operator= (const VectorInt& );  //zdefiniowanie operatora przypisania dla obiektow naszej klasy
    ~VectorInt(); //destruktor
    int at(int);  //wypisuje dany element wektora
    void insert(int, int);  //wpisuje wartosc na dane miejsce w wektorze
    void pushback(int);  //dopisuje wartosc na koncu wektora, czyli dodaje kolejny "wymiar"
    void popback();   //usuwa ostatni element wektora, czyli ostatni "wymiar"
    void shrinkToFit();  //redukuje zaalokowona pamiec tylko do rozmiaru wektora, czyli usuwa ostatnie miejsca zerowe
    void clear();  //czysci (zeruje) wszystkie elementy wektora
    int size();    //zwraca rozmiar wektora
    int capacity();  //zwraca dostepna pamiec

    int get_dlugosc();  //zwraca zaalokowana pamiec
    void realok(int);  //realokuje pamiec do danej wartosci
    void reg_size();   //reguluje rozmiar wektora

 private:
     int *_wek;  //wskaznik, ktory sluzy za tablice dynamiczna
     int _dlugosc;  //przechowuje zaalokowana pamiec
     int _size;  //przechowuje rozmiar wektora
     int _capacity;  //przechowuje wolne miejsce w pamieci (_capacity=_dlugosc-_size)
 };

std::ostream& operator<<(std::ostream&,VectorInt&); //definicja operatora "<<" dla obiektow naszej klasy

#endif
