#pragma once
#include <iostream>

#ifndef ZESP_HPP_INCLUDED
#define ZESP_HPP_INCLUDED


using namespace std;

class Zespolone
{
public:
    double Re, Im;

    Zespolone(double re=1, double im=1);

    void operator=(Zespolone);
    Zespolone operator+(const Zespolone& z) const;//dodawanie dwoch zespolonych
    friend Zespolone operator+(Zespolone, double);//zespolona+re
    friend Zespolone operator+(double, Zespolone);//re+zespolona
    Zespolone operator-(const Zespolone& z) const;//zespolona-zespolona
    friend Zespolone operator-(Zespolone, double);//zespolona-re
    friend Zespolone operator-(double, Zespolone);//re-zespolona
    Zespolone operator*(const Zespolone& z) const;//zespolona*zespolona
    friend Zespolone operator*(Zespolone, double);//zespolona*re
    friend Zespolone operator*(double, Zespolone);//re*zespolona
    Zespolone spr(); //sprzezenie
};

//wypisanie do strumienia ostream
std::ostream& operator<<(std::ostream &out, const Zespolone &c);



#endif // ZESP_HPP_INCLUDED
