#include <iostream>
#include <vector>
#include "zesp.h"

using namespace std;

int main()
{
    Zespolone z1(3, 5);
    Zespolone z2(1, 2);
    Zespolone z3=z1.spr();
    cout << z1+z2 <<"\n"
        << z1+5 <<"\n"
        << z1-z2 <<"\n"
        << z1-1 <<"\n"
        << 5-z1 <<"\n"
        << z1*z2 <<"\n"
        << z1*2 <<"\n"
        << 2*z1 <<"\n"
        << z3 <<"\n";


    return 0;
}