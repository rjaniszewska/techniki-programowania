#include <iostream>
#include "zesp.h"

using namespace std;

Zespolone::Zespolone(double re, double im)
{
    Re=re;
    Im=im;
}

void Zespolone::operator=(Zespolone z)
{

    Re=z.Re;
    Im=z.Im;

}

//dodawanie dwoch zespolonych
Zespolone Zespolone::operator+(const Zespolone& z) const
{
    Zespolone zes;
    zes.Re=z.Re+Re;
    zes.Im=z.Im+Im;
    return zes;
}

//zespolona+re
Zespolone operator+(Zespolone z, double r)
{
    z.Re=z.Re+r;
    z.Im=z.Im;
    return z;
}

//re+zespolona
Zespolone operator+(double r, Zespolone z)
{
    z.Re=r+z.Re;
    z.Im=z.Im;
    return z;
}

Zespolone Zespolone::operator-(const Zespolone& z) const
{
    Zespolone zes;
    zes.Re=Re-zes.Re;
    zes.Im=Im-zes.Im;
    return zes;
}

//zespolona-re
Zespolone operator-(Zespolone z, double r)
{
    z.Re=z.Re-r;
    z.Im=z.Im;
    return z;
}

//re-zespolona
Zespolone operator-(double r, Zespolone z)
{
    z.Re=r-z.Re;
    z.Im=z.Im;
    return z;
}

//zespolona*zespolona
Zespolone Zespolone::operator*(const Zespolone& z) const
{
    Zespolone zes, z1, z2;
    z1.Re=Re*z.Re;
    z1.Im=Re*z.Im;

    z2.Im=Im*z.Re;
    z2.Re=Im*z.Im;
    z2.Re=z2.Re*(-1);

    zes.Re=z1.Re+z2.Re;
    zes.Im=z1.Im+z2.Im;
    return zes;
}

//zespolona*re
Zespolone operator*(Zespolone z, double r)
{
    Zespolone zes;
    zes.Re=z.Re*r;
    zes.Im=z.Im*r;
    return zes;
}

//re*zespolona
Zespolone operator*(double r, Zespolone z)
{
    Zespolone zes;
    zes.Re=r*z.Re;
    zes.Im=r*z.Im;
    return zes;
}

Zespolone Zespolone::spr()
{
    return Zespolone(Re, -Im);
}

std::ostream& operator<<(std::ostream &out, const Zespolone &c)
{
    double re = c.Re;
    double im = c.Im;
    if (im>=0)
    {
        return out << "=>" << re << " + " << im << "i";
    }
    else return out << "=>" << re << " " << im << "i";
}



